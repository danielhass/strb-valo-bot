# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
stages:
- setup
- test
- package
- container
- release
- deploy

variables:
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  PYPROJECT_PACKAGE_VERSION: tbd

.release-rule:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - src/strb_valo_bot/version.py

.non-dev-version-tag-rule:
  rules:
    - if: $CI_COMMIT_TAG =~ /\d*.\d*.\d*$/

renovate:
  stage: test
  image: renovate/renovate:latest
  variables:
    LOG_LEVEL: DEBUG
  script:
    - renovate --platform gitlab --autodiscover $RENOVATE_EXTRA_FLAGS
  only:
    - schedule

sonarcloud-check:
  stage: test
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

setup:
  stage: setup
  image: python:3.12
  script:
    - cd $CI_PROJECT_DIR/src/strb_valo_bot
    - export PACKAGE_VERSION=$(python -c "from version import app_version; print(app_version)")
    - echo "PYPROJECT_PACKAGE_VERSION=${PACKAGE_VERSION}" > pyproject.env
    - echo "TEST=test" >> pyproject.env
    - cat pyproject.env
  artifacts:
    reports:
      dotenv: $CI_PROJECT_DIR/src/strb_valo_bot/pyproject.env
  rules:
    - !reference [.release-rule, rules]
    - !reference [.non-dev-version-tag-rule, rules]

package:
  stage: package
  image: python:3.12
  script:
    - pip install build twine
    - python -m build .
    - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*
  rules:
    - !reference [.release-rule, rules]

build:
  image: docker:27.4.0-cli
  stage: container
  services:
    - docker:27.4.0-dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$PYPROJECT_PACKAGE_VERSION
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker buildx create --use
    - docker buildx build --push --platform linux/amd64,linux/arm64 -t $IMAGE_TAG .
  rules:
    - !reference [.release-rule, rules]

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  dependencies:
    - setup
  script:
    - '[ "$PYPROJECT_PACKAGE_VERSION" = "tbd" ] && { echo "ERROR: Variabel PYPROJECT_PACKAGE_VERSION not set."; exit 1; }'
    - echo "Releasing version $PYPROJECT_PACKAGE_VERSION"
  release:
    tag_name: $PYPROJECT_PACKAGE_VERSION
    name: "v$PYPROJECT_PACKAGE_VERSION"
    description: "Release v$PYPROJECT_PACKAGE_VERSION"
  rules:
    - !reference [.release-rule, rules]

# deploy:
#   stage: deploy
#   image: mcr.microsoft.com/azure-cli
#   script:
#     - '[ "$PYPROJECT_PACKAGE_VERSION" = "tbd" ] && { echo "ERROR: Variabel PYPROJECT_PACKAGE_VERSION not set."; exit 1; }'
#     - echo "Deploying version $PYPROJECT_PACKAGE_VERSION"
#     - az login --service-principal -u $AZURE_APP_ID -p $AZURE_APP_SECRET --tenant $AZURE_TENANT
#     - apk update
#     - apk add envsubst
#     # replace secret env vars in deployment manifest
#     - cat $CI_PROJECT_DIR/manifests/azure/strb-bot-container.yaml | envsubst > deploy.yaml
#     # deploy templates manifest to Azure
#     - az container create --resource-group $AZURE_RESOURCE_GROUP --file deploy.yaml
#   rules:
#     - !reference [.non-dev-version-tag-rule, rules]
