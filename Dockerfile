FROM python:3.12.8-slim-bookworm

ENV DEBIAN_FRONTEND=noninteractive

RUN mkdir /workdir
ADD . /workdir
RUN apt-get update && \
    apt-get install -y vim && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    pip install build && \
    cd /workdir && \
    python -m build . && \
    pip install -U ./dist/*.whl && \
    rm -rf /workdir

RUN groupadd --gid 1000 app && \
    useradd --uid 1000 --gid 1000 -m app

USER 1000
WORKDIR /home/app

CMD [ "strb-valo-bot" ]
