# STRB Valo Bot

[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=danielhass_strb-valo-bot&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=danielhass_strb-valo-bot)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=danielhass_strb-valo-bot&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=danielhass_strb-valo-bot)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=danielhass_strb-valo-bot&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=danielhass_strb-valo-bot)

This is a single chat Telegram chatbot to support Valorant clans with:

- finding times to play
- scheduling of Premier matches
- installed Valorant updates ahead of time

## Thank You!

- Thanks to [Henrik Mertens](https://github.com/Henrik-3) for providing [unofficial-valorant-api](https://github.com/Henrik-3/unofficial-valorant-api) which makes querying game data a breeze.
- Tahnks to the [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot) community for making the creation of Telegram bots in Python so easy.

## Development

### Release

Releases of this project are automated via GitLab CI.

A change to the `src/strb_valo_bot/version.py` will trigger a new release. If the new release verison is not a dev version and matches the following regex `v\d.\d.\d$`, the release will automatically deployed to Azure.

## License

This project is licensed LGPLv3. See [License](LICENSE.txt).
