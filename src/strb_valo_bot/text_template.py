helptext = """
<b>Hi! I'm the STRB Valo Bot.</b>

Available commands:
- <i>/help</i>: Print this help message.
- <i>/setup_timers</i>: Setup regular timer jobs (needed once).
- <i>/game</i>: Send poll for a game.
- <i>/gameLoL</i>: Send poll for a LoL game.
- <i>/poll</i>: Create your own Poll. Syntax caption questions. # indicates the tracked answer.
- <i>/manual_premier</i>: Trigger premier poll manually.
- <i>/manual_update</i>: Check for game update manually.
- <i>/player</i>: Prints the current rank and RR of the player asking.
- <i>/premier_job</i> [true|false]: activates (true) or deactivates (false) the scheduled job for premier.

<i>Currently running v{}</i>
"""

updateText = """
<b>Valorant Update Alert!</b>

New game version {} available.
                """

noUpdateText = """
<b>There is no update</b>
Current game version is {}.
                """

premier = """Upcoming Premier Match Schedule - Map: {}"""

valo_postive_text = """{} wants to gain RR!\n
Rank: <b>{}</b> | Current RR: <b>{}</b> \n
Current party size: <b>{}</b>"""

lol_postive_text = """{} wants to gain LP!\n
Current party size: <b>{}</b>"""

custom_poll_text = """{} joined party \n
Current size: <b>{}</b>"""

permier_text = """Upcoming Premier Match Schedule - Map: {}"""

player_stats_text = """
<b>{}</b>\n
Rank: <b>{}</b> | RR: <b>{}</b>
"""
premierJob = """Premier Job has been set to <b>{}</b>"""

premierJobError = """Unrecognized or empty <i>/premier_job</i> [command] text. Please use true or false."""
