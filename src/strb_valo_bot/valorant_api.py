import requests
import logging
from datetime import datetime, date, timedelta
import dateutil.parser
import pytz
from models import WeeklyGames, Player
from save import SaveDataManager

logger = logging.getLogger(__name__)


class ValorantApi:

    def __init__(self, base_url, version_url: str, save_manager: SaveDataManager):
        self.base_url = base_url
        self.version_url = version_url
        self.save_manager = save_manager
        self.save_data = save_manager.getSaveData()

    @staticmethod
    def _date_in_week(week_start: datetime, week_end: datetime, query: datetime):
        if week_start <= query <= week_end:
            return True
        return False
    
    def _requests_get(self, url: str) -> requests.Response:
        """
        Requests GET wrapper used to:
            - print API errors to the application log
            - provide common request headers
        """
        headers = {
            'Authorization': self.save_data.valorant_api_token
        }

        response = requests.get(url, headers=headers)

        if not response.ok:
            logger.error(f"Valorant API Error Response: \n {response.text}")
        return response

    def getWeeklyPremierSchedule(self):
        logger.info("Query API for premier schedule")
        r = self._requests_get(f"{self.base_url}/premier/seasons/eu")
        premier_data = r.json()
        return self._parse_data(premier_data)

    def getPlayerRank(self, name_tag) -> Player:
        logger.info(f"Query API for {name_tag} rank")
        player = None

        name = name_tag.split("#")[0]
        tag = name_tag.split("#")[1]
        r = self._requests_get(
            f"{self.base_url}/mmr/{self.save_data.valorant_region}/{name}/{tag}"
        )

        player_data = r.json()["data"]
        player = Player(
            name=name_tag,
            rank=player_data["currenttierpatched"],
            rr=player_data["ranking_in_tier"],
        )

        return player

    def getGameVersion(self) -> str:
        logger.info("Query API for current game version")
        r = self._requests_get(f"{self.version_url}/version/eu")
        version_data = r.json()["data"]
        return version_data["build_ver"]

    def _parse_data(self, json_data) -> WeeklyGames:

        weekly_games = WeeklyGames()

        date_today = date.today()
        week_start = date_today - timedelta(days=date_today.weekday())
        week_end = week_start + timedelta(days=6)

        premier_events = json_data["data"]
        game_events = {}
        current_event = None

        for event in premier_events:
            start_date = dateutil.parser.parse(event["starts_at"])
            end_date = dateutil.parser.parse(event["ends_at"])

            if start_date <= pytz.UTC.localize(datetime.now()) <= end_date:
                current_event = event

        if current_event == None:
            # no premiere games scheduled this week
            return weekly_games

        for game in current_event["events"]:

            game_events[game["id"]] = game

            if game["type"] == "LEAGUE":
                start_date = dateutil.parser.parse(game["starts_at"])
                end_date = dateutil.parser.parse(game["ends_at"])

                if week_start <= start_date.date() <= week_end:

                    weekly_games.map = game["map_selection"]["maps"][0]["name"]

        for schedule in current_event["scheduled_events"]:
            if schedule["conference"] == self.save_data.premier_conference:
                if ValorantApi._date_in_week(
                    week_start,
                    week_end,
                    dateutil.parser.parse(schedule["starts_at"]).date(),
                ):
                    if game_events[schedule["event_id"]]["type"] == "LEAGUE":
                        current_game = {
                            "start": dateutil.parser.parse(schedule["starts_at"]),
                            "end": dateutil.parser.parse(schedule["ends_at"]),
                        }
                        # no duplicate events in den poll
                        if not weekly_games.games.__contains__(current_game): 
                            weekly_games.games.append(current_game)
        return weekly_games
