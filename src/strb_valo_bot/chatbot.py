import logging
from datetime import time, datetime, timedelta
from telegram import Update
from telegram.constants import ParseMode
from telegram.ext import Application, CommandHandler, ContextTypes, PollAnswerHandler
from packaging import version
from valorant_api import ValorantApi
from save import SaveDataManager
from version import app_version
import global_const
import text_template as texts

logger = logging.getLogger(__name__)


class ChatBot:

    def __init__(
        self,
        application: Application,
        valoApi: ValorantApi,
        save_manager: SaveDataManager,
    ):
        self.application = application
        self.api = valoApi
        self.save_manager = save_manager
        self.save_data = save_manager.getSaveData()

        application.add_handler(CommandHandler("game", self.generateGamingPoll))
        application.add_handler(CommandHandler("gameLoL", self.generateGamingLoLPoll))
        application.add_handler(CommandHandler("setup_timers", self.setupTimers))
        application.add_handler(
            CommandHandler("manual_premier", self.manualSendPremierSchedule)
        )
        application.add_handler(
            CommandHandler("manual_update", self.manuelUpdateChecker)
        )
        application.add_handler(CommandHandler("help", self.help))
        application.add_handler(CommandHandler("poll", self.generateCustomPoll))
        application.add_handler(CommandHandler("player", self.getPlayerStats))
        application.add_handler(PollAnswerHandler(self.receive_poll_answer))
        application.add_handler(CommandHandler("premier_job", self.premierjob))

    def activateTimers(self, context):

        logger.info("Active timers")

        # a = datetime.now() - timedelta(hours=1)
        # b = a + timedelta(seconds=2)

        if self.save_data.premier_timer_active == True:
            self.primierJob(context)

        context.job_queue.run_repeating(
            callback=self.updateChecker,
            interval=self.save_data.update_checker_interval * 60 * 60,  # 3 hours
            # interval=3,       # 3 sec
            name="valo_update_checker",
            chat_id=self.save_data.chat_id,
        )

        logger.info(context.job_queue.jobs())

    def primierJob(self, context):
        context.job_queue.run_daily(
            callback=self.sendPremierSchedule,
            time=time(
                self.save_data.premier_timer_schedule.hour,
                self.save_data.premier_timer_schedule.minute,
            ),
            # time=b,
            name="premier_schedule",
            chat_id=self.save_data.chat_id,
            days=tuple(self.save_data.premier_timer_schedule.days),
        )

    def _get_telegram_username(self, update: Update) -> str:
        user = None
        if update.effective_user.username:
            user = update.effective_user.username
        elif update.effective_user.full_name:
            user = update.effective_user.full_name
        return user

    async def help(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        await context.bot.send_message(
            context._chat_id,
            texts.helptext.format(app_version),
            parse_mode=ParseMode.HTML,
        )

    async def getPlayerStats(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> None:
        telegram_username = self._get_telegram_username(update)
        player = self.api.getPlayerRank(self.save_data.chat_name_map[telegram_username])
        await context.bot.send_message(
            context._chat_id,
            texts.player_stats_text.format(player.name, player.rank, player.rr),
            parse_mode=ParseMode.HTML,
        )

    async def manuelUpdateChecker(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> None:
        await self.updateChecker(context)

    async def updateChecker(self, context: ContextTypes.DEFAULT_TYPE) -> None:

        logger.info("Running Valorant update checker")

        # Check if job is running or a manuel update
        if context.job is not None:
            chat_id = context.job.chat_id
        else:
            chat_id = context._chat_id

        game_version = self.api.getGameVersion()
        if version.parse(game_version) > version.parse(
            self.save_data.last_game_version
        ):
            await context.bot.send_message(
                chat_id,
                texts.updateText.format(game_version),
                parse_mode=ParseMode.HTML,
            )
            self.save_data.last_game_version = game_version
            self.save_manager.writeSaveData(self.save_data)
        # If User ask for update, than print this message as feedback when there is now update.
        elif context.job is None:
            await context.bot.send_message(
                chat_id,
                texts.noUpdateText.format(game_version),
                parse_mode=ParseMode.HTML,
            )

    async def manualSendPremierSchedule(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> None:
        await self.sendPremierSchedule(context)

    async def sendPremierSchedule(self, context: ContextTypes.DEFAULT_TYPE) -> None:

        logger.info("Generate Premier schedule poll")

        chat_id = self.save_data.chat_id
        if context.job is not None:
            chat_id = context.job.chat_id
        else:
            chat_id = context._chat_id
        options = []
        weekly_games = self.api.getWeeklyPremierSchedule()

        if len(weekly_games.games) == 0:
            # no premier games scheduled this week
            logger.info("No premier schedules found. Continue...")
            return

        for slot in weekly_games.games:
            options.append(f"{slot['start'].astimezone():%a %Y-%m-%d %H:%M}")
        poll_caption = texts.permier_text.format(weekly_games.map)
        await self.generateTelegramPoll(
            global_const.POLL_TYP_CUSTOM,
            options,
            [global_const.TELEGRAM_GAMING_POLL_ANSWER],
            poll_caption,
            True,
            chat_id,
            context,
        )

    async def receive_poll_answer(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> None:
        """Summarize a users poll vote"""

        # stores the users answers
        # key: user
        # value: list of answers
        user_answers = {}
        answer = update.poll_answer
        positive_answered_poll = context.bot_data[answer.poll_id]
        try:
            questions = positive_answered_poll[global_const.TELEGRAM_POLL_QUESTIONS]
            answered_poll_typ = positive_answered_poll[global_const.POLL_TYP]
            tracking_answer = positive_answered_poll[global_const.TRACKING_ANSWER]
            user_answers = positive_answered_poll[
                global_const.TELEGRAM_POLL_USER_ANSWER
            ]
        # this means this poll answer update is from an old poll, we can't do our answering then
        except KeyError:
            return
        selected_options = answer.option_ids
        answer_string = []
        for question_id in selected_options:
            answer_string.append(questions[question_id])

        # Check if user has a telegram userName otherwise use his Fullname
        user = self._get_telegram_username(update)

        # check a answer was given and if the at least one of the answers is a tracked answer
        # set() & set() is true if at least one item is intersecting both lists
        if (len(answer_string) > 0) and (set(answer_string) & set(tracking_answer)):
            # we use the answers field from the bot context to store only positive answers
            positive_answered_poll[global_const.TELEGRAM_POLL_ANSWERS] += 1
            user_answers[user] = answer_string
            if answered_poll_typ == global_const.POLL_TYP_VALO:
                if user in self.save_data.chat_name_map:
                    player = self.api.getPlayerRank(self.save_data.chat_name_map[user])
                    await context.bot.send_message(
                        positive_answered_poll[global_const.TELEGRAM_POLL_CHAT_ID],
                        texts.valo_postive_text.format(
                            update.effective_user.mention_html(),
                            player.rank,
                            player.rr,
                            positive_answered_poll[global_const.TELEGRAM_POLL_ANSWERS],
                        ),
                        parse_mode=ParseMode.HTML,
                    )
                else:
                    logger.info("%s bot is not chat name mapping list", user)
            # if poll is for lol then at the moment just answer with user who wants to play
            elif answered_poll_typ == global_const.POLL_TYP_LOL:
                await context.bot.send_message(
                    positive_answered_poll[global_const.TELEGRAM_POLL_CHAT_ID],
                    texts.lol_postive_text.format(
                        update.effective_user.mention_html(),
                        positive_answered_poll[global_const.TELEGRAM_POLL_ANSWERS],
                    ),
                    parse_mode=ParseMode.HTML,
                )

            elif answered_poll_typ == global_const.POLL_TYP_CUSTOM:
                # we use the answers field from the bot context to store only positive answers
                await context.bot.send_message(
                    positive_answered_poll[global_const.TELEGRAM_POLL_CHAT_ID],
                    texts.custom_poll_text.format(
                        update.effective_user.mention_html(),
                        positive_answered_poll[global_const.TELEGRAM_POLL_ANSWERS],
                    ),
                    parse_mode=ParseMode.HTML,
                )

        # empty list is probably a retracted poll
        elif len(answer_string) == 0:
            user_answer = user_answers[user]
            # if user has retracted answer, decrease party size
            if len(user_answer) > 0 and (set(user_answer) & set(tracking_answer)):
                positive_answered_poll[global_const.TELEGRAM_POLL_ANSWERS] -= 1
            user_answers.pop(user)

        # save user answer if a non-tracked answer as been given
        elif len(answer_string) > 0:
            user_answers[user] = answer_string

    async def generateGamingLoLPoll(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> None:
        """Sends a predefined poll"""
        logger.info("Generate game schedule poll")
        await self.generateTelegramPoll(
            global_const.POLL_TYP_LOL,
            global_const.TELEGRAM_GAMING_POLL_QUESTIONS,
            [global_const.TELEGRAM_GAMING_POLL_ANSWER],
            global_const.TELEGRAM_GAMING_LOL_CAPTION,
            False,
            update.effective_chat.id,
            context,
        )

    async def generateGamingPoll(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> None:
        """Sends a predefined poll"""
        logger.info("Generate game schedule poll")
        await self.generateTelegramPoll(
            global_const.POLL_TYP_VALO,
            global_const.TELEGRAM_GAMING_POLL_QUESTIONS,
            [global_const.TELEGRAM_GAMING_POLL_ANSWER],
            global_const.TELEGRAM_GAMING_CAPTION,
            False,
            update.effective_chat.id,
            context,
        )

    async def generateCustomPoll(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> None:
        logger.info("Generate poll")

        logger.info(update.message.text)
        poll = update.message.text.split()
        # Poll caption is the second Item from the messagetext, the first item is allways the command
        poll_caption = poll[1]
        # Poll questions starts at item postion 2
        questions = []
        track_answer = []
        for idx in range(2, len(poll)):
            if poll[idx].startswith("#"):
                help_str = poll[idx][1 : len(poll[idx])]
                track_answer.append(help_str)
                questions.append(help_str)
            else:
                questions.append(poll[idx])
        await self.generateTelegramPoll(
            global_const.POLL_TYP_CUSTOM,
            questions,
            track_answer,
            poll_caption,
            False,
            update.effective_chat.id,
            context,
        )

    async def generateTelegramPoll(
        self,
        pollTyp: str,
        questions: [],
        track_answer: [],
        poll_caption: str,
        multiple_answers: bool,
        chat_id: str,
        context: ContextTypes.DEFAULT_TYPE,
    ) -> None:
        # Send poll into telegram chat
        message = await context.bot.send_poll(
            chat_id,
            poll_caption,
            questions,
            is_anonymous=False,
            allows_multiple_answers=multiple_answers,
        )
        # Save some info about the poll the bot_data for later use in receive_poll_answer
        payload = {
            message.poll.id: {
                global_const.TELEGRAM_POLL_QUESTIONS: questions,
                global_const.TELEGRAM_POLL_MESSAGE_ID: message.message_id,
                global_const.TELEGRAM_POLL_CHAT_ID: chat_id,
                global_const.TELEGRAM_POLL_ANSWERS: 0,
                global_const.POLL_TYP: pollTyp,
                global_const.TRACKING_ANSWER: track_answer,
                global_const.TELEGRAM_POLL_USER_ANSWER: {},
            }
        }
        context.bot_data.update(payload)

    async def setupTimers(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> None:
        self.save_data.chat_id = update.effective_chat.id
        self.save_manager.writeSaveData(self.save_data)
        self.activateTimers(context)

    async def premierjob(
        self, update: Update, context : ContextTypes.DEFAULT_TYPE
    ) -> None:
        try:
            msg = update.message.text
        except AttributeError:
            logger.warning("Premier job command without message, this might be caused by an edited message.")
            return
        msg = msg.removeprefix("/premier_job ")
        
        current_jobs = context.job_queue.get_jobs_by_name("premier_schedule")
        match msg:
            case "true":
                if len(current_jobs) == 0:
                    # only add job if not already added
                    self.save_data.premier_timer_active = True
                    self.primierJob(context)
            case "false":
                self.save_data.premier_timer_active = False
                for job in current_jobs:
                    # although we should only have one job, iterate to make
                    # sure we remove all jobs
                    job.schedule_removal()
            case _:
                logger.warning("Unrecognized or empty /premier_job command. Please use true or false.")
                await context.bot.send_message(
                    context._chat_id,
                    texts.premierJobError,
                    parse_mode=ParseMode.HTML
                )
                return
            
        self.save_manager.writeSaveData(self.save_data)

        await context.bot.send_message(
            context._chat_id,
            texts.premierJob.format(self.save_data.premier_timer_active),
            parse_mode=ParseMode.HTML,
        )
