from dataclasses import dataclass, field
from dataclasses_json import dataclass_json
from datetime import time


@dataclass
class WeeklyGames:
    """"""

    map: str = ""
    games: list[dict] = field(default_factory=list)


@dataclass
class Player:
    """"""

    name: str
    rank: str
    rr: int


@dataclass_json
@dataclass
class TimerSchedule:
    hour: int
    minute: int
    days: list[int] = field(default_factory=list)


@dataclass_json
@dataclass
class SaveData:
    chat_name_map: dict = field(default_factory=dict)
    last_game_version: str = "0.0"
    chat_id: int = 0
    telegram_token: str = ""
    valorant_api_token: str = ""
    valorant_region : str = "eu"
    premier_conference : str = "EU_DACH"
    premier_timer_active : bool = False
    premier_timer_schedule: TimerSchedule = field(
        default_factory=lambda: TimerSchedule(
            hour=7,
            minute=0,
            # day mapping from 0-6, sun-sat
            days=[1],
        )
    )
    # update interval in hours
    update_checker_interval: int = 3
    
