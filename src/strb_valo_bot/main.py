from telegram.ext import Application
from valorant_api import ValorantApi
from chatbot import ChatBot
from version import app_version
import logging
from save import SaveDataManager

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

# set higher logging level for httpx to avoid all GET and POST requests being logged
logging.getLogger("httpx").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

SAVE_FILE_PATH = ".valo-bot.json"
VALORANT_API_BASE_URL = "https://api.henrikdev.xyz/valorant/v1"
VALORANT_API_VERSION_URL = "https://valorant-api.com/v1/version"


def init_bot():
    """Start the bot."""

    logger.info(f"Starting STRB Valo Bot - v{app_version}")

    save_manager = SaveDataManager(SAVE_FILE_PATH)
    save_data = save_manager.loadSaveData()
    if save_data.telegram_token == "":
        logger.error("Please provide Telegram API token via config file!")
        exit(1)

    api = ValorantApi(VALORANT_API_BASE_URL, VALORANT_API_BASE_URL, save_manager)

    # Create the Application and pass it your bot's token.
    application = Application.builder().token(save_data.telegram_token).build()

    bot = ChatBot(application, api, save_manager)

    if save_data.chat_id != 0:
        bot.activateTimers(application)

    # Run the bot until the user presses Ctrl-C
    application.run_polling(allowed_updates=[])


if __name__ == "__main__":
    init_bot()
