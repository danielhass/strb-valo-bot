# changes to this file will trigger a release!
# if this is not intended please use the following commit message prefix:
#    [ci skip] <commit message>

app_version = "0.20.0"
