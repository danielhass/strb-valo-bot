import json
from models import SaveData


class SaveDataManager:

    def __init__(self, save_file_path: str):
        self.save_file_path = save_file_path
        self.save_data = self.loadSaveData()

    def loadSaveData(self):
        try:
            with open(self.save_file_path) as save_file:
                return SaveData.from_dict(json.load(save_file))
        except FileNotFoundError:
            with open(self.save_file_path, "x") as save_file_new:
                save_file_new.write(SaveData().to_json(indent=2))
            with open(self.save_file_path) as save_file_new:
                return SaveData.from_dict(json.load(save_file_new))

    def getSaveData(self) -> SaveData:
        return self.save_data

    def writeSaveData(self, save_data: SaveData):
        with open(self.save_file_path, "w+") as save_file:
            save_file.write(save_data.to_json(indent=2))
